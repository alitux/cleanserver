# Clean Server

Este script en Python te ayuda a liberar espacio en disco realizando diversas limpiezas, incluyendo el Journal del sistema, logs, caché de Apt, imágenes Docker no utilizadas y volúmenes Docker no utilizados.

## Requisitos

- Python 3.x
- Colorama (podés instalarlo con pip install colorama)
## Instalación
```bash
sudo pip install git+https://gitlab.com/alitux/cleanserver
 ```
## Uso

Ejecutá el script como usuario sudo o root para garantizar los permisos necesarios.
```
sudo cleanserver [argumentos]
```

- -a o --all: Realiza una limpieza profunda que incluye caché, journal y logs.

- -d o --days: Define la antigüedad de los archivos a limpiar (por defecto 7 días).

- -j o --journal: Realiza la limpieza de los logs del Journal.

- -l o --logs: Realiza la limpieza de los logs del sistema.

- -p o --aptclean: Limpia la caché de Apt (solo en sistemas Debian/Ubuntu).

- -u o --umbral: Umbral de disco usado en porcentaje decimal. Si se supera ese valor se activa la limpieza.

- -di o --docker_images: Elimina todas las imágenes Docker no utilizadas.

- -dv o --docker_volumes: Elimina los volúmenes Docker no utilizados.

- -s o --swapoff: Desactiva la memoria swap (No incluído en limpieza profunda)

- -v o --verbose: Muestra la salida estándar de cada comando.

## Ejemplos

### Limpieza profunda del sistema con antiguedad de 7 días
```bash
sudo cleanserver -a
```

### Limpiar solo los logs de los últimos 3 días
```bash
sudo cleanserver -l -d 3
```

### Limpiar caché de Apt en un sistema Debian/Ubuntu
```bash
sudo cleanserver -p
```

## Limpiar todo el sistema cuando el espacio libre en disco sea mayor al 80%
```bash
sudo cleanserver -a -u 80
```



## TODO
- [x] Hacer un paquete instalable vía pip
- [x] Preparar herramienta para analizar espacio en disco y que se lance automáticamente con un umbral
- [ ] En caso de que se pase el umbral si o si debe estar acompañado de **-a, -j, -l, -p, -u, -di, -dv**
- [ ] Llegar a versión estable

## BUGS
- [x] Los stdout de los comandos no se muestran, excepto la salida del journaling.
- [x] El journaling envía prints y no encuentro la forma de ponerlo en silencioso