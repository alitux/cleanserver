import subprocess
import argparse
import psutil
import os
import sys
from colorama import Fore

def disk_space():
    """
    Muestra el espacio libre de /
    """
    for particion in psutil.disk_partitions():
        if particion.mountpoint == '/':
            uso = psutil.disk_usage(particion.mountpoint).percent
    return uso

def umbral_trigger(umbral):
    """
    Comprueba si el espacio libre de / supera el umbral.
    Si Disco > Umbral --> True
    Si Disco < Umbral --> False 
    """ 
    disco = disk_space()
    if disco > umbral: return True
    if disco < umbral: return False

def sudo_test():
    """Comprueba si el usuario es sudo
    """
    if os.geteuid() != 0:
        print(Fore.RED + "El script se debe ejecutar como sudo o root.")
        sys.exit(1)

# Funciones de Limpieza

def journal_clean(days):
    """
    Limpia el Journal de sistema
    """
    comando = f"journalctl --vacuum-time={days}d --quiet"
    resultado = subprocess.run(comando, shell=True, stdout=subprocess.PIPE, text=False, check=True)
    return resultado

def logs_clean(days):
    """Limpia los logs de Sistema
    
    Keyword arguments:
    days -- Días de Antiguedad
    Return: stdout del comando.
    """
    comando = f'find /var/log -type f -name "*.log" -mtime +{days} -exec rm {{}} \;'
    resultado = subprocess.run(comando, shell=True, stdout=subprocess.PIPE, text=True)
    return resultado

def apt_clean():
    """
    Limpia la caché de Apt
    """
    
    comando = "apt-get clean"
    resultado = subprocess.run(comando, shell=True, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL, text=True)
    return resultado

def docker_clean_images():
    """
    Elimina todas las imagenes no utilizadas en Docker
    """
    comando = "docker image prune -f"
    resultado = subprocess.run(comando, shell=True, stdout=subprocess.PIPE, text=True)
    return resultado

def docker_clean_volumes():
    """
    Elimina todos los volúmenes no utilizados en docker
    """
    comando = "docker volume prune -f"
    resultado = subprocess.run(comando, shell=True, stdout=subprocess.PIPE, text=True)
    return resultado

def swap_off():
    comando = f"sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab & sudo swapoff -a"
    resultado = subprocess.run(comando, shell=True, stdout=subprocess.PIPE, text=True)
    resultado = comando
    return resultado


def deep_clean(days, verbose=False):
    """Ejecuta una limpieza profunda del sistema.
    
    Keyword arguments:
    days -- Días de antiguedad
    verbose -- Por defecto en False: Define si se muestra el stdout de los comandos.
    """
    disco_antes = disk_space()
    print(Fore.GREEN + "LIMPIEZA PROFUNDA")
    print(Fore.GREEN + "1. Limpiar Logs de Journal...", end="")
    journal = journal_clean(days)
    if journal.returncode == 0:
        print(Fore.GREEN + "OK")
    else:
        print(Fore.RED + "ERROR")
    if verbose:
        print(journal.stdout)
    print(Fore.GREEN + "2. Limpieza de Logs...", end="")
    logs = logs_clean(days)
    if logs.returncode == 0:
        print(Fore.GREEN + "OK")
    else:
        print(Fore.RED + "ERROR")
    if verbose:
        print(logs.stdout)
    print (Fore.GREEN + "3. Limpieza caché apt...", end="")
    aptclean = apt_clean()
    if aptclean.returncode == 0:
        print(Fore.GREEN + "OK")
    else:
        print(Fore.RED + "ERROR")
    if verbose:
        print(aptclean.stdout)
    print (Fore.GREEN + "4. Docker: Limpieza de Imágenes no utilizadas...", end="")
    docker_images = docker_clean_images()
    if docker_images.returncode == 0:
        print(Fore.GREEN + "OK")
    else:
        print(Fore.RED + "ERROR")   
    if verbose:
        print(docker_images.stdout)
    print (Fore.GREEN + "5. Docker: Limpieza de Volúmenes no utilizados...", end="")
    docker_volumes = docker_clean_volumes()
    if docker_volumes.returncode == 0:
        print(Fore.GREEN + "OK")
    else:
        print(Fore.RED + "ERROR")
    if verbose: 
        print(docker_volumes.stdout)
    print(Fore.YELLOW + f"ESPACIO LIBERADO: {round(disco_antes - disk_space(),2)} %")
    sys.exit (1)

def main():
    """
    Función principal de la herramienta
    """
    
    parser = argparse.ArgumentParser(description="Libera espacio en disco eliminando journal, logs, caché, imágenes docker inutilizadas, volúmenes docker inutilizados, etc.")
    #Argumentos por defecto
    days = 7
    verbose=False
    umbral=None
    # Argumentos aceptados
    parser.add_argument('-a', '--all', action='store_true', help='Limpieza profunda de caché, journal y logs.')
    parser.add_argument('-d', '--days', type=int, help='Antiguedad de los archivos a limpiar. Por defecto 7 días')
    parser.add_argument('-u', '--umbral', type=float, help='Umbral de disco usado en numero decimal(ej: 30). Si se supera ese valor se activa la limpieza')
    parser.add_argument('-j', '--journal', action='store_true', help='Limpieza del Journal del sistema de Archivos')
    parser.add_argument('-l', '--logs', action='store_true', help='Limpieza de logs de sistema')
    parser.add_argument('-p', '--aptclean', action='store_true', help='Limpia el caché de apt (Solo sistemas Debian/Ubuntu)')
    parser.add_argument('-di', '--docker_images', action='store_true', help='Docker: Elimina todas las imágenes no utilizadas')
    parser.add_argument('-dv', '--docker_volumes', action='store_true', help='Docker: Elimina los volúmenes no utilizados')
    parser.add_argument('-s', '--swapoff', action='store_true', help='Desactiva el swap')
    parser.add_argument('-v', '--verbose', action='store_true', help='Salida de stdout para cada comando')
    args = parser.parse_args()
    if not any(vars(args).values()):
        parser.print_help()
        sys.exit(1)
    sudo_test()
    #Días de antiguedad para la limpieza
    if args.days is not None:
        days = args.days
    ## Umbral
    if args.umbral is not None:
        umbral = args.umbral
        umbral_test = umbral_trigger(umbral=umbral)
        if umbral_test is False: sys.exit(1) #Si no se supera el umbral sale del programa.
    #Modo Verbose
    if args.verbose:
        verbose = True
    #Limpieza Profunda
    if args.all:
        deep_clean(days, verbose=verbose)

    # Limpiezas independientes
    disco_antes = disk_space()
    if args.journal:
        print(Fore.GREEN + "Limpieza de Logs de Journal...", end="")
        journal = journal_clean(days)
        if journal.returncode != 0:
            print(Fore.RED + "Error")
        else:
            print(Fore.GREEN + "OK")
        # if args.verbose:
        #     print(journal.stdout)
    if args.logs:
        print(Fore.GREEN + "Limpieza de Logs..", end="")
        logs = logs_clean(days)
        if logs.returncode!= 0:
            print(Fore.RED + "Error")
        else:
            print(Fore.GREEN + "OK")
        if args.verbose:
            print(logs.stdout)
    if args.aptclean:
        print(Fore.GREEN + "Limpieza de Caché APT...", end="")
        aptclean = apt_clean()
        if aptclean.returncode!= 0:
            print(Fore.RED + "Error")
        else:
            print(Fore.GREEN + "OK")
        if args.verbose:
            print(aptclean.stdout)
    if args.docker_images:
        print(Fore.GREEN + "Docker: Limpieza de Imágenes no utilizadas...", end="")
        docker_images_salida = docker_clean_images()
        if docker_images_salida.returncode!= 0:
            print(Fore.RED + "Error")
        else:
            print(Fore.GREEN + "OK")
        if args.verbose:
            print(docker_images_salida.stdout)
    if args.docker_volumes:
        print(Fore.GREEN + "Docker: Limpieza de Volúmenes no utilizados...", end="")
        docker_volumes_salida = docker_clean_volumes()
        if docker_volumes_salida.returncode!= 0:
            print(Fore.RED + "Error")
        else:
            print(Fore.GREEN + "OK")
        if args.verbose: 
            print(docker_volumes_salida.stdout)
    if args.swapoff:
        print(Fore.GREEN + "Desactivando Swap...", end="")
        swapoff = swap_off()
        if swapoff.returncode!= 0:
            print(Fore.RED + "Error")
        else:
            print(Fore.GREEN + "OK")
        if args.verbose:
            print(swapoff.stdout)
    #Solo mostrar si recibió algún argumento distinto a -a
    print(f"ESPACIO LIBERADO: {round(disco_antes - disk_space(),2)} %")
if __name__ == "__main__":
    main()